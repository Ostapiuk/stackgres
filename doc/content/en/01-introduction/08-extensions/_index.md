---
title: "Extensions"
weight: 8
url: intro/extensions
---

The table below contains the list of all avaiable extensions:

<script src="{{<relurl url="/js/stackgres-postgres-extensions-list-v2.js">}}"></script>
<div class="postgresExtensions">Loading extensions info...</div>
