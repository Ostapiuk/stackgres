#!/bin/sh

. "$SPEC_PATH/abstract/metrics"

e2e_exclusive_lock() {
  true
}

e2e_test_install() {
  if ! can_install_operator_version "$STACKGRES_PREVIOUS_VERSION"
  then
    return
  fi

  RANDOM_VALUE="$(random_string)"
  CLUSTER_1_NAME="$(get_sgcluster_name "$CLUSTER_NAME-1")"
  CLUSTER_2_NAME="$(get_sgcluster_name "$CLUSTER_NAME-2")"
  CLUSTER_3_NAME="$(get_sgcluster_name "$CLUSTER_NAME-3")"
  DBOPS_1_NAME="$(get_sgdbops_name "security-upgrade-1")"
  DBOPS_2_NAME="$(get_sgdbops_name "security-upgrade-2")"
  DBOPS_3_NAME="$(get_sgdbops_name "security-upgrade-3")"

  PREVIOUS_PATRONI_IMAGE="$(get_component_images "$STACKGRES_PREVIOUS_VERSION")"
  PREVIOUS_PATRONI_IMAGE="$(printf '%s' "$PREVIOUS_PATRONI_IMAGE" | grep '/patroni\(-ext\)\?:')"
  PREVIOUS_PATRONI_IMAGE="$(printf '%s' "$PREVIOUS_PATRONI_IMAGE" | tail -n 1)"
  PREVIOUS_PATRONI_IMAGE_POSTGRES_VERSION_WITH_BUILD_VERSION="${PREVIOUS_PATRONI_IMAGE##*-pg}"
  PREVIOUS_PATRONI_IMAGE_POSTGRES_VERSION="${PREVIOUS_PATRONI_IMAGE_POSTGRES_VERSION_WITH_BUILD_VERSION%%-build-*}"
  PATRONI_IMAGE="$(get_component_images "$STACKGRES_VERSION" | grep '/patroni\(-ext\)\?:' | grep "[-]pg$PREVIOUS_PATRONI_IMAGE_POSTGRES_VERSION-" | tail -n 1)"

  PREVIOUS_DISTRIBUTEDLOGS_PATRONI_IMAGE="$(get_component_images "$STACKGRES_PREVIOUS_VERSION")"
  PREVIOUS_DISTRIBUTEDLOGS_PATRONI_IMAGE="$(printf '%s' "$PREVIOUS_DISTRIBUTEDLOGS_PATRONI_IMAGE" | grep '/patroni\(-ext\)\?:v[0-9.]\+-pg12[0-9.]\+-build-')"
  PREVIOUS_DISTRIBUTEDLOGS_PATRONI_IMAGE="$(printf '%s' "$PREVIOUS_DISTRIBUTEDLOGS_PATRONI_IMAGE" | tail -n 1)"

  k8s_unnamespaced_cleanup
  k8s_cleanup_namespace "$OPERATOR_NAMESPACE"
  k8s_async_cleanup

  if [ "$E2E_FORCE_IMAGE_PULL" = "true" ]
  then
    if [ "$E2E_SKIP_LOAD_OPERATOR" != true ]
    then
      load_operator_images_from "$E2E_OPERATOR_REGISTRY" "$E2E_OPERATOR_REGISTRY_PATH" \
        "$STACKGRES_PREVIOUS_VERSION"
    fi

    if [ "$E2E_SKIP_LOAD_COMPONENTS" != true ]
    then
      E2E_INCLUDE_ONLY_POSTGRES_VERSIONS= load_component_images_from "$E2E_COMPONENTS_REGISTRY" "$E2E_COMPONENTS_REGISTRY_PATH" \
        "$STACKGRES_PREVIOUS_VERSION"
    fi

    if [ "$E2E_SKIP_LOAD_EXTENSIONS" != true ]
    then
      load_extensions_images_from "$E2E_EXTENSIONS_REGISTRY" "$E2E_EXTENSIONS_REGISTRY_PATH" \
        "$STACKGRES_PREVIOUS_VERSION"
    fi
  fi

  local VERSION_AS_NUMBER VERSIONAS_NUMBER_0_9_5
  VERSION_AS_NUMBER="$(get_version_as_number "$STACKGRES_PREVIOUS_VERSION")"
  VERSION_0_9_5="$(get_version_as_number 0.9.5)"
  if [ "$VERSION_AS_NUMBER" -gt "$VERSION_0_9_5" ]
  then
    install_prometheus_operator
  else
    install_prometheus_operator_for_version 12.8.0
  fi
  install_operator_previous_version \
    --set grafana.autoEmbed=true \
    --set-string grafana.webHost="prometheus-grafana.$(prometheus_namespace)"

  kubectl create namespace "$CLUSTER_NAMESPACE"

  install_minio

  DISTRIBUTED_LOGS_NAME="$(get_sgdistributedlogs_name distributedlogs)"
  create_or_replace_cluster_for_version "$STACKGRES_PREVIOUS_VERSION" "$CLUSTER_NAME" "$CLUSTER_NAMESPACE" 1 \
    --set cluster.create=false \
    --set-string cluster.postgres.version="$PREVIOUS_PATRONI_IMAGE_POSTGRES_VERSION" \
    --set instanceProfiles[0].name=size-xs \
    --set instanceProfiles[0].cpu=250m \
    --set instanceProfiles[0].memory=512Mi \
    --set-string configurations.backupconfig.baseBackups.cronSchedule='0 5 31 2 *' \
    --set-string cluster.configurations.sgBackupConfig=backupconf \
    --set configurations.backupconfig.create=true \
    --set-string configurations.postgresconfig.postgresql\.conf.max_connections=100 \
    --set distributedLogs.enabled=true \
    --set distributedLogs.create=true \
    --set-string cluster.distributedLogs.sgDistributedLogs="$DISTRIBUTED_LOGS_NAME" \
    --set-string distributedLogs.persistentVolume.size=128Mi

  create_or_replace_cluster_for_version "$STACKGRES_PREVIOUS_VERSION" \
    "$CLUSTER_1_NAME" "$CLUSTER_NAMESPACE" 1 \
    --set configurations.create=false --set instanceProfiles=false \
    --set-string cluster.postgres.version="$PREVIOUS_PATRONI_IMAGE_POSTGRES_VERSION" \
    --set prometheusAutobind=true \
    --set backupconfig.create=true \
    --set backupconfig.retention=2 \
    --set-string cluster.configurations.sgBackupConfig=backupconf \
    --set distributedLogs.enabled=true \
    --set distributedLogs.create=false \
    --set-string cluster.distributedLogs.sgDistributedLogs="$CLUSTER_NAMESPACE.$DISTRIBUTED_LOGS_NAME"

  create_or_replace_cluster_for_version "$STACKGRES_PREVIOUS_VERSION" \
    "$CLUSTER_2_NAME" "$CLUSTER_NAMESPACE" 2 \
    --set configurations.create=false --set instanceProfiles=false \
    --set-string cluster.postgres.version="$PREVIOUS_PATRONI_IMAGE_POSTGRES_VERSION" \
    --set prometheusAutobind=true \
    --set backupconfig.create=true \
    --set backupconfig.retention=2 \
    --set-string cluster.configurations.sgBackupConfig=backupconf \
    --set distributedLogs.enabled=true \
    --set distributedLogs.create=false \
    --set-string cluster.distributedLogs.sgDistributedLogs="$CLUSTER_NAMESPACE.$DISTRIBUTED_LOGS_NAME"

  create_or_replace_cluster_for_version "$STACKGRES_PREVIOUS_VERSION" \
    "$CLUSTER_3_NAME" "$CLUSTER_NAMESPACE" 3 \
    --set configurations.create=false --set instanceProfiles=false \
    --set-string cluster.postgres.version="$PREVIOUS_PATRONI_IMAGE_POSTGRES_VERSION" \
    --set prometheusAutobind=true \
    --set backupconfig.create=true \
    --set backupconfig.retention=2 \
    --set-string cluster.configurations.sgBackupConfig=backupconf \
    --set distributedLogs.enabled=true \
    --set distributedLogs.create=false \
    --set-string cluster.distributedLogs.sgDistributedLogs="$CLUSTER_NAMESPACE.$DISTRIBUTED_LOGS_NAME"

  deploy_curl_pod "$CLUSTER_NAMESPACE"

  wait_pods_running "$CLUSTER_NAMESPACE" 1 "$CLUSTER_1_NAME-[0-9]\+"

  BACKUP_NAME="$(get_sgbackup_name "$CLUSTER_NAME-backup-1")"

  cat << EOF | kubectl create -f -
apiVersion: stackgres.io/$(kubectl get crd sgbackups.stackgres.io --template '{{ (index .spec.versions 0).name }}')
kind: SGBackup
metadata:
  namespace: "$CLUSTER_NAMESPACE"
  name: "$BACKUP_NAME"
spec:
  sgCluster: "$CLUSTER_1_NAME"
  managedLifecycle: false
EOF
  
  wait_until is_backup_phase "Completed"

  BACKUP_UID="$(kubectl get sgbackup -n "$CLUSTER_NAMESPACE" "$BACKUP_NAME" --template='{{ .metadata.uid }}')"

  remove_cluster "$CLUSTER_1_NAME" "$CLUSTER_NAMESPACE"
  create_or_replace_cluster_for_version "$STACKGRES_PREVIOUS_VERSION" \
    "$CLUSTER_1_NAME" "$CLUSTER_NAMESPACE" 1 \
    --set configurations.create=false --set instanceProfiles=false \
    --set-string cluster.postgres.version="$PREVIOUS_PATRONI_IMAGE_POSTGRES_VERSION" \
    --set prometheusAutobind=true \
    --set backupconfig.create=true \
    --set backupconfig.retention=2 \
    --set-string cluster.configurations.sgBackupConfig=backupconf \
    --set-string cluster.initialData.restore.fromBackup.uid="$BACKUP_UID" \
    --set-string cluster.metadata.labels.clusterPods.pod-label="$RANDOM_VALUE" \
    --set-string cluster.metadata.annotations.clusterPods.pod-annotation="$RANDOM_VALUE" \
    --set-string cluster.metadata.annotations.primaryService.primary-service-label="$RANDOM_VALUE" \
    --set-string cluster.metadata.annotations.replicasService.replicas-service-label="$RANDOM_VALUE" \
    --set distributedLogs.enabled=true \
    --set distributedLogs.create=false \
    --set-string cluster.distributedLogs.sgDistributedLogs="$CLUSTER_NAMESPACE.$DISTRIBUTED_LOGS_NAME"

  wait_pods_running "$CLUSTER_NAMESPACE" 9

  wait_cluster "$CLUSTER_1_NAME" "$CLUSTER_NAMESPACE"
  wait_cluster "$CLUSTER_2_NAME" "$CLUSTER_NAMESPACE"
  wait_cluster "$CLUSTER_3_NAME" "$CLUSTER_NAMESPACE"
  wait_cluster "$DISTRIBUTED_LOGS_NAME" "$CLUSTER_NAMESPACE"

  generate_mock_data "$CLUSTER_1_NAME"
  generate_mock_data "$CLUSTER_2_NAME"
  generate_mock_data "$CLUSTER_3_NAME"

  CLUSTER_CRD="sgclusters.stackgres.io"
}


is_backup_phase() {
  [ "$(kubectl get sgbackup -n "$CLUSTER_NAMESPACE" "$BACKUP_NAME" -o=jsonpath='{.status.process.status}')" = "$1" ]
}

e2e_test_uninstall() {
  if ! can_install_operator_version "$STACKGRES_PREVIOUS_VERSION"
  then
    return
  fi

  uninstall_prometheus_operator
  delete_operator_only
  install_operator_only
  wait_pods_running "$OPERATOR_NAMESPACE" 2
}

e2e_test() {
  if ! can_install_operator_version "$STACKGRES_PREVIOUS_VERSION"
  then
    echo "Skip dbops-security-upgrade since previous version of operator $STACKGRES_PREVIOUS_VERSION can not be installed"
    return
  fi
  run_test "Check clusters before operator upgrade" check_before_operator_upgrade
  run_test "Check that operator can be upgraded to newer version" check_operator_upgrade
  run_test "Check that previous CRDs be converted to previous versions" check_previous_versions_conversion_webhooks
  run_test "Check that distributedlogs node can be security upgrade after operator upgrade" check_distributedlogs_security_upgrade
  run_test "Check that cluster with 1 node can start security upgrade after operator upgrade with reduced impact" check_cluster_1_security_upgrade_start
  run_test "Check that cluster with 2 node can start security upgrade after operator upgrade with reduced impact" check_cluster_2_security_upgrade_start
  run_test "Check that cluster with 3 node can start security upgrade after operator upgrade with in-place" check_cluster_3_security_upgrade_start
  run_test "Check that cluster with 1 node can complete security upgrade after operator upgrade with reduced impact" check_cluster_1_security_upgrade
  run_test "Check that cluster with 2 node can complete security upgrade after operator upgrade with reduced impact" check_cluster_2_security_upgrade
  run_test "Check that cluster with 3 node can complete security upgrade after operator upgrade with in-place" check_cluster_3_security_upgrade
  run_test "Checking that metrics are exported for cluster with 1 node" check_metrics "$CLUSTER_1_NAME"
  run_test "Checking that metrics are exported for cluster with 2 node" check_metrics "$CLUSTER_2_NAME"
  run_test "Checking that metrics are exported for cluster with 3 node" check_metrics "$CLUSTER_3_NAME"
  run_test "Check that the conversion webhooks are configured" check_conversion_webhooks_configured
}

check_before_operator_upgrade() {
  check_mock_data_samehost "$CLUSTER_1_NAME"
  check_mock_data "$CLUSTER_2_NAME"
  check_mock_data "$CLUSTER_3_NAME"

  local RESOURCE
  for RESOURCE in \
    "sgcluster/$CLUSTER_1_NAME" \
    "sgcluster/$CLUSTER_2_NAME" \
    "sgcluster/$CLUSTER_3_NAME" \
    "sgdistributedlogs/$DISTRIBUTED_LOGS_NAME"
  do
    if ! kubectl wait -n "$CLUSTER_NAMESPACE" "$RESOURCE" --for condition=PendingRestart --timeout 0
    then
      echo "SUCCESS. $RESOURCE is not pending restart after creation"
    else
      echo "FAIL. $RESOURCE is pending restart after creation"
      return 1
    fi
  done
}

check_operator_upgrade() {
  local POD_OPERATOR_IMAGE
  POD_OPERATOR_IMAGE="$(kubectl get pod -n "$OPERATOR_NAMESPACE" -l app=stackgres-operator \
    --template '{{ range .items }}{{ if not .metadata.deletionTimestamp }}{{ range .spec.containers }}{{ printf "%s\n" .image }}{{ end }}{{ end }}{{ end }}' \
    | grep '/operator:')"
  if [ "$POD_OPERATOR_IMAGE" = "$STACKGRES_PREVIOUS_OPERATOR_IMAGE" ]
  then
    echo "SUCCESS. Operator pod is using the previous operator image"
  else
    echo "FAILURE. Operator pod is not using the previous operator image"
    echo
    echo "Previous operator image is $STACKGRES_PREVIOUS_OPERATOR_IMAGE"
    echo
    echo "Used operator image is $POD_OPERATOR_IMAGE"
    return 1
  fi
  local POD_RESTAPI_IMAGE
  POD_RESTAPI_IMAGE="$(kubectl get pod -n "$OPERATOR_NAMESPACE" -l app=stackgres-restapi \
    --template '{{ range .items }}{{ if not .metadata.deletionTimestamp }}{{ range .spec.containers }}{{ printf "%s\n" .image }}{{ end }}{{ end }}{{ end }}' \
    | grep '/restapi:')"
  if [ "$POD_RESTAPI_IMAGE" = "$STACKGRES_PREVIOUS_RESTAPI_IMAGE" ]
  then
    echo "SUCCESS. Operator pod is using the previous restapi image"
  else
    echo "FAILURE. Operator pod is not using the previous restapi image"
    echo
    echo "Previous restapi image is $STACKGRES_PREVIOUS_RESTAPI_IMAGE"
    echo
    echo "Used restapi image is $POD_RESTAPI_IMAGE"
    return 1
  fi

  upgrade_operator --reset-values \
    --set grafana.autoEmbed=true \
    --set-string grafana.webHost="prometheus-grafana.$(prometheus_namespace)"

  POD_OPERATOR_IMAGE="$(kubectl get pod -n "$OPERATOR_NAMESPACE" -l app=stackgres-operator \
    --template '{{ range .items }}{{ if not .metadata.deletionTimestamp }}{{ range .spec.containers }}{{ printf "%s\n" .image }}{{ end }}{{ end }}{{ end }}' \
    | grep '/operator:' | sed 's#^[^/]\+/\([^/]\+/[^:]\+\)#\1#')"
  if [ "$POD_OPERATOR_IMAGE" = "$STACKGRES_OPERATOR_IMAGE" ]
  then
    echo "SUCCESS. Operator pod is using the new operator image"
  else
    echo "FAILURE. Operator pod is not using the new operator image"
    echo
    echo "New operator image is $STACKGRES_OPERATOR_IMAGE"
    echo
    echo "Used operator image is $POD_OPERATOR_IMAGE"
    return 1
  fi
  POD_RESTAPI_IMAGE="$(kubectl get pod -n "$OPERATOR_NAMESPACE" -l app=stackgres-restapi \
    --template '{{ range .items }}{{ if not .metadata.deletionTimestamp }}{{ range .spec.containers }}{{ printf "%s\n" .image }}{{ end }}{{ end }}{{ end }}' \
    | grep '/restapi:' | sed 's#^[^/]\+/\([^/]\+/[^:]\+\)#\1#')"
  if [ "$POD_RESTAPI_IMAGE" = "$STACKGRES_RESTAPI_IMAGE" ]
  then
    echo "SUCCESS. Operator pod is using the new restapi image"
  else
    echo "FAILURE. Operator pod is not using the new restapi image"
    echo
    echo "New restapi image is $STACKGRES_RESTAPI_IMAGE"
    echo
    echo "Used restapi image is $POD_RESTAPI_IMAGE"
    return 1
  fi
}

check_previous_versions_conversion_webhooks() {
  kubectl proxy --port=9090 &
  KUBECTL_PROXY_PID=$!
  trap_kill "$KUBECTL_PROXY_PID"
  ANY_CONVERSION_FAILED=false
  for CRD_NAME in sgclusters sginstanceprofiles \
    sgpgconfigs sgpoolconfigs sgbackupconfigs \
    sgbackups sgdbops sgdistributedlogs
  do
    for PREVIOUS_API_VERSION in $(kubectl get crd "$CRD_NAME.stackgres.io" \
      -o=jsonpath='{ .spec.versions[?(@.storage != true)].name }')
    do
      if curl -f -s -k -X GET  -H "Accept: application/json" \
        "http://localhost:9090/apis/stackgres.io/$PREVIOUS_API_VERSION/$CRD_NAME" > /dev/null
      then
        echo "SUCCESS. $CRD_NAME.stackgres.io can be converted to $PREVIOUS_API_VERSION"
      else
        echo "FAIL. $CRD_NAME.stackgres.io can not be converted to $PREVIOUS_API_VERSION"
        ANY_CONVERSION_FAILED=true
      fi
    done
  done
  kill "$KUBECTL_PROXY_PID"
  if "$ANY_CONVERSION_FAILED"
  then
    return 1
  fi
}

check_distributedlogs_security_upgrade() {
  local CLUSTER_CRD="sgdistributedlogs.stackgres.io"
  local CLUSTER_NAME="$DISTRIBUTED_LOGS_NAME"
  local PREVIOUS_PATRONI_IMAGE="$PREVIOUS_DISTRIBUTEDLOGS_PATRONI_IMAGE"

  check_cluster_before_security_upgrade "$DISTRIBUTED_LOGS_NAME"

  kubectl annotate sgdistributedlogs.stackgres.io -n "$CLUSTER_NAMESPACE" "$DISTRIBUTED_LOGS_NAME" \
    --overwrite "stackgres.io/operatorVersion=$(get_installed_operator_version)"
  kubectl delete sts -n "$CLUSTER_NAMESPACE" "$DISTRIBUTED_LOGS_NAME"

  wait_pods_running "$CLUSTER_NAMESPACE" 1 "$DISTRIBUTED_LOGS_NAME-[0-9]\+"
  wait_cluster "$DISTRIBUTED_LOGS_NAME" "$CLUSTER_NAMESPACE"

  check_cluster_after_security_upgrade "$DISTRIBUTED_LOGS_NAME"
}

check_cluster_1_security_upgrade_start() {
  kubectl delete sgbackup -n "$CLUSTER_NAMESPACE" "$BACKUP_NAME"
  check_cluster_security_upgrade_start "$CLUSTER_1_NAME" "$DBOPS_1_NAME" "ReducedImpact"
}

check_cluster_2_security_upgrade_start() {
  check_cluster_security_upgrade_start "$CLUSTER_2_NAME" "$DBOPS_2_NAME" "ReducedImpact"
}

check_cluster_3_security_upgrade_start() {
  check_cluster_security_upgrade_start "$CLUSTER_3_NAME" "$DBOPS_3_NAME" "InPlace"
}

check_cluster_1_security_upgrade() {
  check_cluster_security_upgrade "$CLUSTER_1_NAME" "$DBOPS_1_NAME" "ReducedImpact"
}

check_cluster_2_security_upgrade() {
  check_cluster_security_upgrade "$CLUSTER_2_NAME" "$DBOPS_2_NAME" "ReducedImpact"
}

check_cluster_3_security_upgrade() {
  check_cluster_security_upgrade "$CLUSTER_3_NAME" "$DBOPS_3_NAME" "InPlace"
}

check_cluster_security_upgrade_start() {
  local CLUSTER_NAME="$1"
  local DBOPS_NAME="$2"
  local METHOD="$3"
  shift 3

  check_cluster_before_security_upgrade

  check_mock_data_samehost "$CLUSTER_NAME"

  cat << EOF | kubectl create -f -
apiVersion: stackgres.io/v1
kind: SGDbOps
metadata:
  name: $DBOPS_NAME
  namespace: $CLUSTER_NAMESPACE
spec:
  sgCluster: $CLUSTER_NAME
  op: securityUpgrade
  securityUpgrade:
    method: $METHOD
EOF

  assert_dbops_running "$DBOPS_NAME" "$CLUSTER_NAMESPACE"

  wait_until eval '[ "$(kubectl get sgcluster -n "$CLUSTER_NAMESPACE" "$CLUSTER_NAME" \
    --template "{{ if .metadata.annotations.lockTimestamp }}{{ .metadata.annotations.lockTimestamp }}{{ else }}0{{ end }}")" != 0 ]'
  if kubectl patch sgcluster -n "$CLUSTER_NAMESPACE" "$CLUSTER_NAME" --type json \
    -p '[{"op":"replace","path":"/spec/metadata","value":{"annotations":{"allResources":{"'"$(random_string)"'": "'"$(random_string)"'"}}}}]' \
    >/dev/null 2>&1
  then
    echo "FAILED. Cluster has been updated while locked."
    return 1
  else
    echo "SUCCESS. Cluster has not been updated while locked."
  fi
}

check_cluster_security_upgrade() {
  local CLUSTER_NAME="$1"
  local DBOPS_NAME="$2"
  local METHOD="$3"
  shift 3

  assert_dbops_completion "$DBOPS_NAME" "$CLUSTER_NAMESPACE" "$((E2E_TIMEOUT * 2))"

  check_cluster_after_security_upgrade

  check_mock_data_samehost "$CLUSTER_NAME"
}

check_cluster_before_security_upgrade() {
  if wait_until eval 'kubectl wait "$CLUSTER_CRD" -n "$CLUSTER_NAMESPACE" "$CLUSTER_NAME" --for condition=PendingRestart --timeout 0'
  then
    echo "SUCCESS. Cluster $CLUSTER_NAME is pending restart after operator upgrade"
  else
    echo "FAIL. Cluster $CLUSTER_NAME is not pending restart after operator upgrade"
    return 1
  fi

  check_sts_is_not_altered "$CLUSTER_NAME"

  local POD
  local PODS
  PODS="$(kubectl get pod -n "$CLUSTER_NAMESPACE" \
    -l "app=StackGresCluster,cluster-name=$CLUSTER_NAME,cluster=true" -o name \
    | cut -d / -f 2)"
  for POD in $PODS
  do
    POD_PATRONI_IMAGE="$(kubectl get pod -n "$CLUSTER_NAMESPACE" "$POD" \
      --template '{{ range .spec.containers }}{{ printf "%s\n" .image }}{{ end }}' \
       | grep '/patroni\(-ext\)\?:')"
    if [ "$POD_PATRONI_IMAGE" = "$PREVIOUS_PATRONI_IMAGE" ]
    then
      echo "SUCCESS. Pod $POD is using the previous patroni image"
    else
      echo "FAILURE. Pod $POD is not using the previous patroni image"
      echo
      echo "Previous patroni image is $PREVIOUS_PATRONI_IMAGE"
      echo
      echo "Used patroni image is $POD_PATRONI_IMAGE"
      return 1
    fi
  done
}

check_cluster_after_security_upgrade() {
  if wait_until eval '! kubectl wait "$CLUSTER_CRD" -n "$CLUSTER_NAMESPACE" "$CLUSTER_NAME" --for condition=PendingRestart --timeout 0'
  then
    echo "SUCCESS. Cluster $CLUSTER_NAME is not pending restart after security upgrade"
  else
    echo "FAIL. Cluster $CLUSTER_NAME is pending restart after security upgrade"
    return 1
  fi

  local STS_UPDATE_REVISION
  STS_UPDATE_REVISION="$(wait_until kubectl get sts -n "$CLUSTER_NAMESPACE" "$CLUSTER_NAME" --template '{{ .status.updateRevision }}')"
  local POD_CONTROLLER_REVISION_HASH
  local POD_CONTROLLER_REVISION_HASHES
  POD_CONTROLLER_REVISION_HASHES="$(kubectl get pod -n "$CLUSTER_NAMESPACE" \
      -l "app=StackGresCluster,cluster-name=$CLUSTER_NAME,cluster=true" -o json \
    | jq ".items[]|select(.metadata.name | startswith(\"$CLUSTER_NAME\"))" \
    | jq -r '.metadata.labels."controller-revision-hash"')"

  for POD_CONTROLLER_REVISION_HASH in $POD_CONTROLLER_REVISION_HASHES
  do
    if [ "$POD_CONTROLLER_REVISION_HASH" != "$STS_UPDATE_REVISION" ]
    then
      echo "FAILURE. Cluster $CLUSTER_NAME security upgrade did not updated sucesfully some pods"
      return 1
    fi
  done

  PODS="$(kubectl get pod -n "$CLUSTER_NAMESPACE" \
    -l "app=StackGresCluster,cluster-name=$CLUSTER_NAME,cluster=true" -o name \
    | cut -d / -f 2)"
  for POD in $PODS
  do
    POD_PATRONI_IMAGE="$(kubectl get pod -n "$CLUSTER_NAMESPACE" "$POD" \
      --template '{{ range .spec.containers }}{{ printf "%s\n" .image }}{{ end }}' \
       | grep '/patroni\(-ext\)\?:')"
    if [ "$POD_PATRONI_IMAGE" = "$PATRONI_IMAGE" ]
    then
      echo "SUCCESS. Pod $POD is using the latest patroni image"
    else
      echo "FAILURE. Pod $POD is not using the latest patroni image"
      echo
      echo "New patroni images is '$PATRONI_IMAGE'"
      echo
      echo "Used patroni image is '$POD_PATRONI_IMAGE'"
      return 1
    fi
  done

  local PRIMARY_SERVICE_TYPE
  PRIMARY_SERVICE_TYPE="$(kubectl get service -n "$CLUSTER_NAMESPACE" "$CLUSTER_NAME-primary" \
    --template '{{ .spec.type }}')"
  if [ "$PRIMARY_SERVICE_TYPE" = "ExternalName" ]
  then
    echo "SUCCESS. Cluster $CLUSTER_NAME primary service is of type ExternalName"
  else
    echo "FAILURE. Cluster $CLUSTER_NAME primary service is not of type ExternalName"
    return 1
  fi
}

check_conversion_webhooks_configured(){
  CONVERSTION_STRATEGY="$(kubectl get crd sgclusters.stackgres.io -o jsonpath='{.spec.conversion.strategy}')"

  assert_string_equal "Webhook" "$CONVERSTION_STRATEGY"

  CONVERSTION_STRATEGY="$(kubectl get crd sgdistributedlogs.stackgres.io -o jsonpath='{.spec.conversion.strategy}')"

  assert_string_equal "Webhook" "$CONVERSTION_STRATEGY"
}

check_sts_is_not_altered() {
  local TARGET_CLUSTER="$1"

  local STS_PATRONI_IMAGE
  STS_PATRONI_IMAGE="$(wait_until kubectl get sts -n "$CLUSTER_NAMESPACE" "$TARGET_CLUSTER" -o jsonpath='{.spec.template.spec.containers[0].image}')"

  if assert_string_equal "$PREVIOUS_PATRONI_IMAGE" "$STS_PATRONI_IMAGE"
  then
    echo "SUCCESS. StatefulSet $TARGET_CLUSTER is not being altered on operator upgrade"
  else 
    echo "FAIL. StatefulSet $TARGET_CLUSTER is being altered on operator upgrade"
    return 1
  fi
}
